package ma.optique.optique.administration.service;

import java.util.Map;

public interface EmailService {

	 void sendMail(String to, String subject, String templatePath, Map templateVariable);
	
}
